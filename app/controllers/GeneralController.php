<?php 
	class GeneralController{
		protected $event;
		protected $model;
		protected $view;

		public function __construct(){
			$this->get_event('identidad');
			require 'app/Config.php';
			require 'app/models/GeneralModel.php';
			require 'app/views/GeneralView.php';
		}

		//Get the event from the url, receiving the parameter get event
		protected function get_event($default){
			if(!isset($_GET['event'])){
				$this->event = $default;
			}
			else{
				$this->event = $_GET['event'];
			}
		}

	}
?>