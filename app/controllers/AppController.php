<?php 
	class AppController extends GeneralController{
		public function __construct(){
			parent::__construct();
			require 'app/models/AppModel.php';
			require 'app/views/AppView.php';
			$this->view = new AppView();
			$this->model = new AppModel();

			switch ($this->event) {
				case 'clave':
					$this->model->validate_identity();
					$this->view->print_clave();
				break;
				case 'home' :
					$this->view->print_home();
				break;
				case 'premios' :
					$this->view->print_premios();
				break;
				case 'contacto' :
					$this->view->print_contacto();
				break;
				default:
					$this->model->close_session();
					$this->view->print_indentidad();
				break;
			}
		}
	}

?>