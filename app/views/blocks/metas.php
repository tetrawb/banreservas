<div class="row-produc">
  <div class="item">
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/iconsmartphone.png" alt="">
    </div>
    <div class="info-prem">
      <div class="block" >
        <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
      </div>
      <p class="metaCard">Meta 1</p>
      <p class="subTitleMetaCard text-bold">Tecnología</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir btn-warning width90 text-center bgSilver positionBtnMetas">
        <span class="textRedimir color-White">Canjeado</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/icons-eat.png" alt="">
    </div>
    <div class="info-prem">
      <div class="block" >
        <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
      </div>
      <p class="metaCard">Meta 2</p>
      <p class="subTitleMetaCard text-bold">Entretenimiento</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir bgFuchsia btn-warning width90 text-center positionBtnMetas">
        <span class="textRedimir color-White">A&uacute;n no puedes canjearlo</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/icons-chef.png" alt="">
    </div>
    <div class="info-prem">
      <div class="block" >
        <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
      </div>
      <p class="metaCard">Meta 3</p>
      <p class="subTitleMetaCard text-bold">Comida</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a href="<?php echo $this->general_configuration['url_base'] ?>/premios"  class="btn btnRedimir bgorange btn-warning width90 text-center positionBtnMetas">
        <span class="textRedimir color-White">Canjea tu premio</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="block active" >
      <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
    </div>
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/icons-price-tag.png" alt="">
    </div>
    <div class="info-prem">
      <p class="metaCard">Meta 4</p>
      <p class="subTitleMetaCard text-bold">Vestuario</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir bgFuchsia btn-warning width90 text-center positionBtnMetas">
        <span class="textRedimir color-White">A&uacute;n no puedes canjearlo</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="block active" >
      <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
    </div>
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/iconsmartphone.png" alt="">
    </div>
    <div class="info-prem">
      <p class="metaCard">Meta 1</p>
      <p class="subTitleMetaCard text-bold">Tecnología</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir bgSilver btn-warning width90 text-center  positionBtnMetas">
        <span class="textRedimir color-White">Canjeado</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="block active" >
      <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
    </div>
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/icons-eat.png" alt="">
    </div>
    <div class="info-prem">
      <p class="metaCard">Meta 2</p>
      <p class="subTitleMetaCard text-bold">Entretenimiento</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir bgFuchsia btn-warning width90 text-center positionBtnMetas">
        <span class="textRedimir color-White">A&uacute;n no puedes canjearlo</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="block active" >
      <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
    </div>
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/icons-chef.png" alt="">
    </div>
    <div class="info-prem">
      <p class="metaCard">Meta 3</p>
      <p class="subTitleMetaCard text-bold">Comida</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir bgorange btn-warning width90 text-center positionBtnMetas">
        <span class="textRedimir color-White">Canjea tu premio</span>
      </a>
    </div>
  </div>
  <div class="item">
    <div class="block active" >
      <img class="btnmore" src="<?php echo $url_sources ?>/images/lockedBlock.png" alt="">
    </div>
    <div class="offer rounded-circle badge-danger top-left position-absolute w-25 m-3">
      <img src="<?php echo $url_sources ?>/images/icons-price-tag.png" alt="">
    </div>
    <div class="info-prem">
      <p class="metaCard">Meta 4</p>
      <p class="subTitleMetaCard text-bold">Vestuario</p>
      <p class="fechaMetaCard">*Fecha - fecha</p>
    </div>
    <div class="content-btn">
      <a  class="btn btnRedimir bgFuchsia btn-warning width90 text-center positionBtnMetas">
        <span class="textRedimir color-White">A&uacute;n no puedes canjearlo</span>
      </a>
    </div>
  </div>
</div>
