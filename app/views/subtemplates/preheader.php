<!doctype html>
<html>
<head>
  <meta charset=utf-8>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="#">
  <title>BanReserva</title>
  <link rel="icon" href="<?php echo $url_sources ?>/images/favicon.png">
  <!--Bootstrap core CSS -->
  <link rel="stylesheet" href="<?php echo $url_sources ?>/css/main.css">
</head>
<body>
  <!--HEADER //-->
  <header class="header">
    <div class="container">
      <div class="">
        <div class="row">
          <nav class="navbar navbar-default menuinicial">
            <div class="container-fluid">
              <div class="navbar-header content-logo-princ">
                <a class="navbar-brand masterlogo" href="#">
                  <img alt="" src="<?php echo $url_sources ?>/images/logomaster.png">
                </a>
                <a class="navbar-brand banlogo" href="#">
                  <img alt="" src="<?php echo $url_sources ?>/images/logotipo.png">
                </a>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
