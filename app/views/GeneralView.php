<?php 
	class GeneralView{
		public $general_configuration;

		public function __construct(){
			$config = new Config('view');
			$this->general_configuration = $config->get_settings();
		}

		protected function get_template($template){
			$url_base = $this->general_configuration['url_base'];
			$url_sources = $this->general_configuration['url_sources'];
			require 'app/views/templates/'.$template.'.php';
		}
	}
?>