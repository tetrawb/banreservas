$('document').ready(function(){
	 $('.formlogin form').submit(function(evento){//numerio campo
	 	evento.preventDefault()
	 	auto = $(this)
	      var doc= $('input[name="documento-identidad"]').val();
	      if ($.isNumeric(doc)) {//validamos que sea numerico
	      		$('.msj').remove()
	             if (doc.length > 5) {
	               url = '/banreservas/api/validar_documento';
	             	datos = {
	             		id : doc
	             	};
	             	submit = $.getJSON(url, datos, function(data){
	             		console.log(data)
	             		if(data.status=="200"){
	             			$(auto).unbind('submit')
	             			$(auto).submit()
	             		}else{
	             			console.log('error')
	                  		$('<span id="msj" class="msj">Por favor revisa el documento</span>').insertAfter($("input[name='documento-identidad']"))
	             		}
	             	});

	         }else{
	            $('<span id="msj" class="msj">El número ingresado no es valido</span>').insertAfter($("input[name='documento-identidad']"))
	         }
	      }

	});
})