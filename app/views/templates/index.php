<?php include './app/views/subtemplates/preheader.php'; ?>
  <!--HEADER // -->
  <!--//BANNER -->
  <section>
    <div class="jumbotron jumbologin sliders">
      <div class="landing-container-princ">
        <div class="container">
          <div class="row">
            <!--Colum izq -->
            <div class="col-md-7 col-sm-12 col-xs-12 col-slider-iz">
              <div class="col-md-8 col-sm-8 col-xs-12 col-xss-12">
                <div class="contenedor-text-izq">
                  <?php include 'app/views/blocks/tplista.php'; ?>
                </div>
              </div>

              <div class="col-md-4 col-sm-4 hidden-xs">
                &nbsp;
              </div>
            </div>

            <!--Colum Der -->
            <div class="col-md-5 col-sm-12 col-xs-12 col-slider-de">
              <div  class="img-slider-prehome">
                <img src="<?php echo $url_sources ?>/images/logoslider.png" alt="">
              </div>

              <div class="titbanner1">
                <h2>Inicia sesi&oacute;n y <br>consulta tu meta</h2>
              </div>
              <div class="formlogin">
                <form action="<?php echo $url_base ?>/clave" method="post" class="form-inline">
                  <div class="form-group formuDi">
                    <input type="number" class="form-control input-lg formdoc" id="numberdoc" placeholder="Documento de Identidad" name="documento-identidad" required>
                  </div>
                  <button type="submit" class="btn boton btn-default">Siguiente <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--// BANNER  -->
  <!--FOOTER // -->
  <?php include './app/views/subtemplates/footer.php'; ?>

</body>
</html>
