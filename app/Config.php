<?php 
	class Config{
		public $settings;

		public function __construct($request){
			$this->settings = $this->get_general_settings();

			switch($request){
				case 'database' :
					$this->get_settings_database();
				break;
			}
		}

		private function get_general_settings(){
			return array(
				'url_base' => '/banreservas',
				'url_sources' => '/banreservas/app/views/sources'
			);
		}

		private function get_settings_database(){
			$this->settings['database'] = array(
				'host' => 'localhost',
				'user' => 'root',
				'password' => '',
				'name_database' => 'banreservas'
			);
		}

		public function get_settings(){
			return $this->settings;
		}
	}
?>