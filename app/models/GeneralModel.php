<?php 
	class GeneralModel{
		protected $general_configuration;
		protected $database;

		public function __construct(){
			session_start();
			header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			require 'DatabaseChef.php';
			$config = new Config('model');
			$general_configuration = $config->get_settings();
			$this->database = new DatabaseChef();
		}

		public function close_session(){
			if(isset($_SESSION['id'])){
				session_start();
				session_unset();
				session_destroy();
				session_start();
				session_regenerate_id(true);
				header('Location: '.$this->general_configuration['url_base']);
				exit;
			}
		}

		private function test_input($data, $cant) {
	        $tama = strlen(stripslashes(htmlspecialchars($data)));
	        $data = trim($data);
	    		if (preg_match("/(>|=|<+)/", $data)  OR $tama!=$cant  OR is_int($data) ) {
	            header('Location: '.$this->general_configuration['url_base']);
	            exit;
	    		}else{
	            return $data;
	        }
	  	}
	}
?>